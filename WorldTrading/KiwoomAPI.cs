﻿using AxKFOpenAPILib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

class KiwoomAPI
{
    public static AxKFOpenAPI Get => SingletoneLazy<APIContext>.Instance.kiwoom;

    private sealed class APIContext : Control
    {
        internal AxKFOpenAPI kiwoom;
        private APIContext()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            kiwoom = new AxKFOpenAPI();
            kiwoom.BeginInit();
            SuspendLayout();
            kiwoom.Dock = DockStyle.Fill;
            Controls.Add(kiwoom);
            kiwoom.EndInit();
            ResumeLayout(false);
        }
    }
}

